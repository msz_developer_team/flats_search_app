import { MongoObservable } from 'meteor-rxjs';
import { Meteor } from 'meteor/meteor';
import {FlatToBuy} from "../models/flat.to.buy.model";

export const FlatsToBuyCollection = new MongoObservable.Collection<FlatToBuy>('flat_to_buy');

function loggedIn() {
  return !!Meteor.user();
}

FlatsToBuyCollection.allow({
  insert: loggedIn,
  update: loggedIn,
  remove: loggedIn
});