import {FlatToBuy} from "../../../both/models/flat.to.buy.model";
import {FlatsToBuyCollection} from "../../../both/collections/flats.to.buy.collection";

export class FlatsToBuyDao {

    public save(flatToBuy: FlatToBuy) {
        console.log('save ' + JSON.stringify(flatToBuy));
        FlatsToBuyCollection.collection.insert(flatToBuy);
    }

    public saveAll(flatsToBuy: FlatToBuy[]) {
        flatsToBuy.forEach((flatToBuy: FlatToBuy) => {
            this.save(flatToBuy);
        })
    }

}