import {FlatsToRentDataProvider} from "./flats.to.rent.data.provider";
import {DGDataProvider} from "./domgratka/dg.data.provider";
import {MorizonDataProvider} from "./morizon/morizon.data.provider";
import {ODDataProvider} from "./otodom/od.data.provider";
import {GumtreeDataProvider} from "./gumtree/gumtree.data.provider";
export class FlatsToRentService {

    private dataProviders: FlatsToRentDataProvider[];

    public constructor() {
        this.dataProviders.push(new DGDataProvider());
        this.dataProviders.push(new MorizonDataProvider());
        this.dataProviders.push(new ODDataProvider());
        this.dataProviders.push(new GumtreeDataProvider());
    }

}