import {FlatsToBuyDataProvider} from "../flats.to.buy.data.provider";
import {FlatToBuy} from "../../../../../both/models/flat.to.buy.model";
export class DGDataProvider implements FlatsToBuyDataProvider {
    getProviderName(): string {
        return 'Dom Gratka Data Provider';
    }
    getData(dataToStoreCallback: (error: boolean, resultData: FlatToBuy[]) => void, finishedCallback: (error: boolean) => void) {
        finishedCallback(false);
    }

}