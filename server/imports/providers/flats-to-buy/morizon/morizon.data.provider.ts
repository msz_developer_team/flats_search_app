import {FlatsToBuyDataProvider} from "../flats.to.buy.data.provider";
import {FlatToBuy} from "../../../../../both/models/flat.to.buy.model";
export class MorizonDataProvider implements FlatsToBuyDataProvider {
    getProviderName(): string {
        return 'Morizon Data provider';
    }
    getData(dataToStoreCallback: (error: boolean, resultData: FlatToBuy[]) => void, finishedCallback: (error: boolean) => void) {
        finishedCallback(false);
    }

}