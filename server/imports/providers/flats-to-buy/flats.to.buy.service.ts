import {FlatsToBuyDataProvider} from "./flats.to.buy.data.provider";
import {DGDataProvider} from "./domgratka/dg.data.provider";
import {MorizonDataProvider} from "./morizon/morizon.data.provider";
import {ODDataProvider} from "./otodom/od.data.provider";
import {FlatToBuy} from "../../../../both/models/flat.to.buy.model";
export class FlatsToBuyService {

    private dataProviders: FlatsToBuyDataProvider[] = [];
    private launchedDataProviderIndex = 0;

    public launchDataProvider(providerIndex: number, partiallyFetchedDataCallback: (partiallyFetchedData: FlatToBuy[]) => void, finishedDataFetchCallback: (error: boolean) => void) {
        if(providerIndex == this.dataProviders.length) {
            finishedDataFetchCallback(false);
            return;
        }
        console.log('Launch flats to buy data provider: ' + this.dataProviders[providerIndex].getProviderName());
        this.dataProviders[providerIndex].getData(
            (error: boolean, partialData: FlatToBuy[]) => {
                if(error) {
                    console.error('Error while getting data from provider');
                } else {
                    partiallyFetchedDataCallback(partialData);
                }
            },
            (error: boolean) => {
                this.launchedDataProviderIndex++;
                this.launchDataProvider(this.launchedDataProviderIndex, partiallyFetchedDataCallback, finishedDataFetchCallback);
            });
    }

    public constructor() {
        this.dataProviders.push(new DGDataProvider());
        this.dataProviders.push(new MorizonDataProvider());
        this.dataProviders.push(new ODDataProvider());
    }

    public fetchFlatsData(partiallyFetchedDataCallback: (partiallyFetchedData: FlatToBuy[]) => void, finishedDataFetchCallback: (error: boolean) => void) {
        this.launchedDataProviderIndex = 0;
        this.launchDataProvider(this.launchedDataProviderIndex, partiallyFetchedDataCallback, finishedDataFetchCallback);
    }

}