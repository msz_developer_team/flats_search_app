import {FlatToBuy} from "../../../../both/models/flat.to.buy.model";
export interface FlatsToBuyDataProvider {
    getProviderName(): string;
    getData(dataToStoreCallback: (error: boolean, resultData: FlatToBuy[]) => void, finishedCallback: (error: boolean) => void);
}