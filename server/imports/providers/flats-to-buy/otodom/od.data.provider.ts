import {FlatsToBuyDataProvider} from "../flats.to.buy.data.provider";
import {FlatToBuy} from "../../../../../both/models/flat.to.buy.model";

declare function scrapeODFlatsToBuyData(dataToStoreCallback: (error: boolean, dataToStore: FlatToBuy[]) => void, finishedCallback: (error: boolean) => void): void;

export class ODDataProvider implements FlatsToBuyDataProvider {
    getProviderName(): string {
        return 'OtoDom Data Provider';
    }
    public getData(dataToStoreCallback: (error: boolean, resultData: FlatToBuy[]) => void, finishedCallback: (error: boolean) => void) {
        scrapeODFlatsToBuyData(
            (error: boolean, resultData: any) => {
                dataToStoreCallback(error, resultData);
            },
            (error: boolean) => {
                finishedCallback(error);
            });
    }

}