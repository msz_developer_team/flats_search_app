const Crawler = require("crawler");
const HtmlToJson = require("html-to-json");
const url = require('url');

(function () {
    this.partialDataToReturnLength = 10;
    this.numberOfFetchedData = 0;
    this.results = [];
    this.c = new Crawler({
        maxConnections: 10,
        rateLimit: 5 * 1000,
        callback: function (error, res, done) {
            if (error) {
                console.log(error);
            } else {
                console.log('received response');
                const $ = res.$;
                const nextPageHref = $("div.listing > div > div.row > div.col-md-content > div.after-offers > nav.pull-left > form > ul.pager > li.pager-next > a")['0'].attribs.href;
                const flatsToBuyOffersHtml = $("div.listing > div > div.row > div.col-md-content").html();

                const flatDetailsParser = HtmlToJson.createParser(['body.detailpage > div.article-offer', {
                    'title': function ($details) {
                        console.log($details);
                        console.log(JSON.stringify($details));
                        return '';
                    }
                }]);

                HtmlToJson.parse(flatsToBuyOffersHtml, {
                    'flatOfferUrls': ['article', function ($article) {
                        return $article.attr('data-url');
                    }]
                }, function (error, result) {
                    console.log('Result with detail urls ' + JSON.stringify(result.flatOfferUrls));
                    for (var index = 0; index < result.flatOfferUrls.length; index++) {
                        HtmlToJson.request(result.flatOfferUrls[index], {
                            'title': ['html > body.detailpage > div.article-offer > section.section-offer-title > div > div.row > header > h1', function ($h1) {
                                return $h1.text();
                            }],
                            'addressDistrict': [],
                            'addressStreet': [],
                            'price': [],
                            'pricePerSquareMeter': [],
                            'area': [],
                            'roomsNumber': [],
                            'floor': [],
                            'description': []
                        }, function (error, results) {
                            console.log('details results ' + JSON.stringify(results));
                        })
                    }
                });

                if (nextPageHref === undefined || numberOfFetchedData >= 50) {
                    console.log('done processing');
                    dataScrappingFinishedCallback(false, results);
                } else {
                    console.log('results length ' + results.length);
                    console.log(nextPageHref);
                    c.queue(nextPageHref);
                }
            }
            done();
        }
    });
    this.partialDataRetrievedCallback = function (error, results) {
        console.log('Callback for partial data to return, using default')
    };
    this.dataScrappingFinishedCallback = function (error, results) {
        console.log('Callback for data scrapping not given, using default')
    };
    this.scrapeODFlatsToBuyData = function (partialDataRetrievedCallback, finishedCallback) {
        console.log('get data from service');
        this.numberOfFetchedData = 0;
        this.results = [];
        this.partialDataRetrievedCallback = partialDataRetrievedCallback;
        this.dataScrappingFinishedCallback = finishedCallback;
        this.c.queue('https://www.otodom.pl/sprzedaz/mieszkanie/warszawa/?search%5Bdescription%5D=1&search%5Bdist%5D=0&search%5Bsubregion_id%5D=197&search%5Bcity_id%5D=26&nrAdsPerPage=72');
    };
}).call(this);