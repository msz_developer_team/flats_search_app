import {Meteor} from "meteor/meteor";
import {loadParties} from "./imports/fixtures/parties";
import "./imports/publications/parties";
import "./imports/publications/users";
import "../both/methods/parties.methods";
import {FlatToBuy} from "../both/models/flat.to.buy.model";
import {FlatsToBuyService} from "./imports/providers/flats-to-buy/flats.to.buy.service";
import {FlatsToBuyDao} from "./imports/dao/flats.to.buy.dao";

Meteor.startup(() => {
    loadParties();
});
Meteor.setTimeout(() => {
    let flatsToBuyDao = new FlatsToBuyDao();
    let flatsToBuyService = new FlatsToBuyService();
    flatsToBuyService.fetchFlatsData(Meteor.bindEnvironment((resultData: FlatToBuy[]) => {
            console.log('partial data retrieved');
            flatsToBuyDao.saveAll(resultData);
        }),
        (error: boolean) => {
            console.log('Finished fetching data')
        });
}, 5000);
