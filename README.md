Angular version: 4.2.0-rc.2
Meteor version: 1.5

To execute:

1) Unzip node_modules.zip   (avoids npm dependencies just in case?)

2) Execute command:
 > meteor reset

3) Execute command:
 > meteor

Launch app from: http://localhost:3000/

